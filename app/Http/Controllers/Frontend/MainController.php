<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Post;
use App\Enews;

class MainController extends Controller
{
    public function index()
    {
        //get post
        $hotnews = Post::published()
            ->where('featured', 1)
            ->offset(0)
            ->limit(3)
            ->order()
            ->publicat()
            ->get();

        //get Customer
        $lastnews = Post::published()
            ->offset(0)
            ->limit(6)
            ->order()
            ->publicat()
            ->get();

        $popularnews =  Post::published()
            ->orWhere('featured', 0)
            ->orWhere('featured', 1)
            ->offset(0)
            ->limit(3)
            ->ordermax()
            ->publicat()
            ->get();

        $allnews =  Post::published()
            ->orWhere('featured', 0)
            ->orWhere('featured', 1)
            ->offset(0)
            ->limit(12)
            ->order()
            ->inRandomOrder()
            ->publicat()
            ->get();

        $e_newsall = Enews::published()->publicat()->offset(0)
            ->limit(3)->orderBy('public_at', 'desc')->get();

        $categories = Category::all();

        return view(
            'frontend.home',
            compact('hotnews', 'lastnews', 'popularnews', 'e_newsall', 'allnews', 'categories')
        );
    }

    public function show(Request $request, $slug)
    {
        $e_news =  Enews::where('slug', $slug)->first();
        return view('frontend.e-book.e-news', compact('e_news'));
    }

    public function ebook(Request $request)
    {
        $e_news =  Enews::first();
        return view('frontend.e-book.e-news', compact('e_news'));
    }

    public function search(Request $request)
    {
        $q = $request->search;
        $seachs = Post::where('title', 'LIKE', '%' . $q . '%')->orWhere('excerpt', 'LIKE', '%' . $q . '%')->get();
        if (count($seachs) > 0) {
            return view('frontend.search', compact('seachs', 'q'));
        }
    }
}
