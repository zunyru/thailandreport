@push('css')
.main-menu,#mobile-nav,.latest-post-area .latest-post-wrap .cat-title,.single-sidebar-widget .title,.popular-post-wrap
.title,
.relavent-story-post-wrap .title{
background-color:{{ setting('site.menu_bar_background_color')? setting('site.menu_bar_background_color') : '#04091e' }};
}
@endpush
<header>
    <div class="logo-wrap">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-4 col-md-4 col-sm-12 logo-left no-padding">
                    <a href="{{ url('/') }}">
                        <img class="img-fluid"
                            src="{{ setting('site.logo') ? Voyager::image(setting('site.logo')) : asset('img/logo.png') }}"
                            width="600">
                    </a>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 logo-right no-padding ads-banner">
                    <img class="img-fluid" src="{{ asset('img/air-asia.jpg')  }} " alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid main-menu" id="main-menu">
        <div class="row container align-items-center justify-content-between">
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="{{ url('/') }}">หน้าแรก</a></li>
                    <li><a href="{{ route('e-book.index') }}">หนังสือพิมพ์</a></li>
                    <li><a href="{{ route('news.category','ทั่วไป') }}">ทั่วไป</a></li>
                    <li><a href="{{ route('news.category','การเมือง') }}">การเมือง</a></li>
                    <li><a href="{{ route('news.category','เศรษฐกิจ') }}">เศรษฐกิจ</a></li>
                    <li><a href="{{ route('news.category','ไลฟ์สไตล์-บันเทิง') }}">ไลฟ์สไตล์-บันเทิง</a></li>
                    <li class="menu-has-children"><a href="#">ภาคใต้</a>
                        <ul>
                            <li><a href="{{ route('news.category','กระบี่') }}">กระบี่</a></li>
                            <li><a href="{{ route('news.category','ชุมพร') }}">ชุมพร</a></li>
                            <li><a href="{{ route('news.category','ตรัง') }}">ตรัง</a></li>
                            <li><a href="{{ route('news.category','นครศรีธรรมราช') }}">นครศรีธรรมราช</a></li>
                            <li><a href="{{ route('news.category','นราธิวาส') }}">นราธิวาส</a></li>
                            <li><a href="{{ route('news.category','ปัตตานี') }}">ปัตตานี</a></li>
                            <li><a href="{{ route('news.category','พังงา') }}">พังงา</a></li>
                            <li><a href="{{ route('news.category','พัทลุง') }}">พัทลุง</a></li>
                            <li><a href="{{ route('news.category','ภูเก็ต') }}">ภูเก็ต</a></li>
                            <li><a href="{{ route('news.category','ยะลา') }}">ยะลา</a></li>
                            <li><a href="{{ route('news.category','ระนอง') }}">ระนอง</a></li>
                            <li><a href="{{ route('news.category','สงขลา') }}">สงขลา</a></li>
                            <li><a href="{{ route('news.category','สตูล') }}">สตูล</a></li>
                            <li><a href="{{ route('news.category','สุราษฎร์ธานี') }}">สุราษฎร์ธานี</a></li>
                        </ul>
                    </li>
                    <li class="menu-has-children"><a href="#">ต่างประเทศ</a>
                        <ul>
                            <li><a href="{{ route('news.category','คนใต้ต่างแดน') }}">คนใต้ต่างแดน</a></li>
                            <li><a href="{{ route('news.category','ต่างประเทศ-ทั่วไป') }}">ทั่วไป</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ route('news.category','ภาพข่าวสังคม')}}">ภาพข่าวสังคม</a></li>
                    <li><a href="{{ route('about-us.index')}}">เกี่ยวกับเรา</a></li>
                    <li><a href="{{ route('contact-us.index')}}">ติดต่อเรา</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
