<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('plugin/flipbook/css/flipbook.style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugin/flipbook/css/font-awesome.css')}}">
    <script src="{{ asset('plugin/flipbook/js/flipbook.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
        $(".book-1").flipBook({
            pages:[
                {src:"{{ asset('plugin/flipbook/images/book1/1.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/1.jpg') }}", title:"Cover"},
                {src:"{{ asset('plugin/flipbook/images/book1/2.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/2.jpg') }}", title:"Page two"},
                {src:"{{ asset('plugin/flipbook/images/book1/3.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/3.jpg') }}", title:"Page three"},
                {src:"{{ asset('plugin/flipbook/images/book1/4.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/4.jpg') }}", title:""},
                {src:"{{ asset('plugin/flipbook/images/book1/5.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/5.jpg') }}", title:"Page five"},
                {src:"{{ asset('plugin/flipbook/images/book1/6.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/6.jpg') }}", title:"Page six"},
                {src:"{{ asset('plugin/flipbook/images/book1/7.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/7.jpg') }}", title:"Page seven"},
                {src:"{{ asset('plugin/flipbook/images/book1/8.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/8.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/9.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/9.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/10.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/10.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/11.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/11.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/12.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/12.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/13.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/13.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/14.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/14.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/15.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/15.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/16.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/16.jpg') }}'", title:"Last"},
            ],
            lightBox:true
        });

        $(".book-2").flipBook({
            pages:[
                {src:"images/book2/page1.jpg", thumb:"images/book2/thumb1.jpg", title:"Cover"},
                {src:"images/book2/page2.jpg", thumb:"images/book2/thumb2.jpg", title:"Page two"},
                {src:"images/book2/page3.jpg", thumb:"images/book2/thumb3.jpg", title:"Page three"},
                {src:"images/book2/page4.jpg", thumb:"images/book2/thumb4.jpg", title:""},
                {src:"images/book2/page5.jpg", thumb:"images/book2/thumb5.jpg", title:"Page five"},
                {src:"images/book2/page6.jpg", thumb:"images/book2/thumb6.jpg", title:"Page six"},
                {src:"images/book2/page7.jpg", thumb:"images/book2/thumb7.jpg", title:"Page seven"},
                {src:"images/book2/page8.jpg", thumb:"images/book2/thumb8.jpg", title:"Last"}
            ],
            lightBox:true
        });

        $(".book-3").flipBook({
            pages:[
                {src:"images/book2/page1.jpg", thumb:"images/book2/thumb1.jpg", title:"Cover"},
                {src:"images/book2/page2.jpg", thumb:"images/book2/thumb2.jpg", title:"Page two"},
                {src:"images/book2/page3.jpg", thumb:"images/book2/thumb3.jpg", title:"Page three"},
                {src:"images/book2/page4.jpg", thumb:"images/book2/thumb4.jpg", title:""},
                {src:"images/book2/page5.jpg", thumb:"images/book2/thumb5.jpg", title:"Page five"},
                {src:"images/book2/page6.jpg", thumb:"images/book2/thumb6.jpg", title:"Page six"},
                {src:"images/book2/page7.jpg", thumb:"images/book2/thumb7.jpg", title:"Page seven"},
                {src:"images/book2/page8.jpg", thumb:"images/book2/thumb8.jpg", title:"Last"}
            ],
            lightBox:true
        });

        $(".book-4").flipBook({
            pages:[
                {src:"images/book2/page1.jpg", thumb:"images/book2/thumb1.jpg", title:"Cover"},
                {src:"images/book2/page2.jpg", thumb:"images/book2/thumb2.jpg", title:"Page two"},
                {src:"images/book2/page3.jpg", thumb:"images/book2/thumb3.jpg", title:"Page three"},
                {src:"images/book2/page4.jpg", thumb:"images/book2/thumb4.jpg", title:""},
                {src:"images/book2/page5.jpg", thumb:"images/book2/thumb5.jpg", title:"Page five"},
                {src:"images/book2/page6.jpg", thumb:"images/book2/thumb6.jpg", title:"Page six"},
                {src:"images/book2/page7.jpg", thumb:"images/book2/thumb7.jpg", title:"Page seven"},
                {src:"images/book2/page8.jpg", thumb:"images/book2/thumb8.jpg", title:"Last"}
            ],
            lightBox:true
        });

    })
    </script>
    <style type="text/css">
        .bookshelf .thumb {
            display: inline-block;
            cursor: pointer;
            margin: 0px 0.5%;
            width: 15% !important;
            box-shadow: 0px 1px 3px rgba(0, 0, 0, .3);
            max-width: 100px;
        }

        .bookshelf .thumb img {
            width: 100%;
            display: block;
            vertical-align: top;
        }

        .bookshelf .shelf-img {
            z-index: 0;
            height: auto;
            max-width: 100%;
            vertical-align: top;
            margin-top: -12px;
        }

        .bookshelf .covers {
            width: 100%;
            height: auto;
            z-index: 99;
            position: relative;
            text-align: center;
        }

        .bookshelf {
            text-align: center;
            padding: 0px;
        }
    </style>
</head>

<body>
    <div class="bookshelf">
        <div class="covers">
            <div class="thumb book-1"><img src="images/book1/1.jpg"></div>
        </div>
        <img class="shelf-img" src="images/shelf_wood.png">
    </div>
    <div class="bookshelf">
        <div class="covers">
            <div class="thumb book-1"><img src="images/book2/thumb1.jpg"></div>
            <div class="thumb book-2"><img src="images/book2/thumb1.jpg"></div>
            <div class="thumb book-3"><img src="images/book2/thumb1.jpg"></div>
        </div>
        <img class="shelf-img" src="images/shelf_glass.png">
    </div>
    <div class="bookshelf">
        <div class="covers">
            <div class="thumb book-1"><img src="images/book2/thumb1.jpg"></div>
            <div class="thumb book-2"><img src="images/book2/thumb1.jpg"></div>
            <div class="thumb book-3"><img src="images/book2/thumb1.jpg"></div>
            <div class="thumb book-4"><img src="images/book2/thumb1.jpg"></div>
        </div>
        <img class="shelf-img" src="images/shelf_metal.png">
    </div>
</body>

</html>
