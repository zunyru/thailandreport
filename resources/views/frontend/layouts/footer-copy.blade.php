@push('css')
.page-footer{
background-color:{{ setting('site.footer_background_color') ? setting('site.footer_background_color') : '#1C2331' }};
}
.top-nav-collapse{
}
footer.page-footer a {
color: {{ setting('site.footer_text_color') ? setting('site.footer_text_color') : '#fff' }}; ;
}
@endpush
<!--Footer-->
<footer class="page-footer text-center font-small  wow fadeIn">
    <!--Call to action-->
    <div class="container">
        <div class="row text-center d-flex justify-content-center pt-5 mb-3">
            {{ menu('web','menu.my_footer') }}
        </div>
    </div>
    <!--/.Call to action-->
    <hr class="my-4">
    <!-- Social icons -->
    <div class="pb-4">
        @if(setting('contact.Facebook'))
        <a href="{{ setting('contact.Facebook') }}" target="_blank">
            <i class="fab fa-facebook-f mr-3"></i>
        </a>
        @endif
        @if(setting('contact.Line'))
        <a href="{{ setting('contact.Line') }}" target="_blank">
            <i class="fab fa-line mr-3"></i>
        </a>
        @endif
        @if(setting('contact.twitter'))
        <a href="{{ setting('contact.twitter') }}" target="_blank">
            <i class="fab fa-twitter mr-3"></i>
        </a>
        @endif
        @if(setting('contact.youtube'))
        <a href="{{ setting('contact.youtube') }}" target="_blank">
            <i class="fab fa-youtube mr-3"></i>
        </a>
        @endif
        @if(setting('contact.google-plus'))
        <a href="{{ setting('contact.google-plus') }}" target="_blank">
            <i class="fab fa-google-plus-g mr-3"></i>
        </a>
        @endif
        {{-- <a href="https://dribbble.com/mdbootstrap" target="_blank">
            <i class="fab fa-dribbble mr-3"></i>
        </a> --}}
        @if(setting('contact.pinterest'))
        <a href="{{setting('contact.pinterest') }}" target="_blank">
            <i class="fab fa-pinterest mr-3"></i>
        </a>
        @endif
        @if(setting('contact.linked-in'))
        <a href="{{ setting('contact.linked-in') }}" target="_blank">
            <i class="fab fa-linkedin mr-3"></i>
        </a>
        @endif
        @if(setting('contact.github'))
        <a href="{{ setting('contact.github')}}" target="_blank">
            <i class="fab fa-github mr-3"></i>
        </a>
        @endif
        @if(setting('contact.github'))
        <a href="{{ setting('contact.github')}}" target="_blank">
            <i class="fab fa-gitlab mr-3"></i>
        </a>
        @endif
        @if(setting('contact.bitbucket'))
        <a href="{{ setting('contact.bitbucket')}}" target="_blank">
            <i class="fab fa-bitbucket mr-3"></i>
        </a>
        @endif
        @if(setting('contact.git'))
        <a href="{{ setting('contact.git')}}" target="_blank">
            <i class="fab fa-git mr-3"></i>
        </a>
        @endif
    </div>
    <!-- Social icons -->
    <!--Copyright-->
    <div class="footer-copyright py-3">
        © 2020 Copyright:
        <a href="{{ url('/') }}" target="_blank">{{ env('APP_NAME') }} </a>
    </div>
    <!--/.Copyright-->
</footer>
<!--/.Footer-->
