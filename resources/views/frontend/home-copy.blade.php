@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')
<main class="">
    <div class="container">
        @if(sizeof($services) > 0)
        <!--Section: Images-->
        <section class="text-center">
            <h1 class="h3 text-left mb-3">บริการ</h1>
            <!-- Grid row -->
            <div class="row">
                @foreach ($services as $service)
                <!-- Grid column -->
                <div class="col-lg-4 col-md-6 mb-3">
                    <a class="d-block rounded-lg lift lift-lg" href="{{ route('services.show',$service->slug) }}">
                        <img src="{{ Voyager::image($service->thumbnail('medium')) }}" class="img-fluid rounded-lg"
                            alt="{{ $service->title }}">
                    </a>
                    <a href="{{ route('services.show',$service->slug) }}">
                        <h1 class="h5 text-center mt-3">{{ $service->title }}</h1>
                    </a>
                </div>
                <!-- Grid column -->
                @endforeach
            </div>
            <!-- Grid row -->
        </section>
        <!--Section: Images-->
        @endif
        @if(sizeof($posts) > 0)
        <section class="pt-5">
            <h1 class="h3 text-left mb-3">บทความ</h1>
            @foreach ($posts as $post)
            <!--Grid row-->
            <div class="row wow fadeIn">
                <!--Grid column-->
                <div class="col-lg-5 col-xl-4 mb-4">
                    <!--Featured image-->
                    <div class="view overlay rounded z-depth-1">
                        <img src="{{ Voyager::image($post->thumbnail('medium')) }}" class="img-fluid"
                            alt="{{ $post->title }}">
                        <a href="{{ route('articles.show',$post->slug) }}" target="_self">
                            <div class="mask rgba-white-slight"></div>
                        </a>
                    </div>
                </div>
                <!--Grid column-->
                <!--Grid column-->
                <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                    <a href="{{ route('articles.show',$post->slug) }}" target="_self">
                        <h3 class="mb-3 font-weight-bold dark-grey-text">
                            <strong>{{ $post->title }}</strong>
                        </h3>
                    </a>
                    <p class="grey-text">{{ Str::limit($post->excerpt,220) }}</p>
                    <a href="{{ route('articles.show',$post->slug) }}" target="_self"
                        class="btn btn-primary btn-md">อ่านเพิ่มเติม
                        <i class="fas fa-play ml-2"></i>
                    </a>
                </div>
                <!--Grid column-->
            </div>
            <!--Grid row-->
            <hr class="mb-5">
            @endforeach
        </section>
        <!--Section: More-->
        @endif
        @if(sizeof($customers) > 0)
        <section>
            <h2 class="my-2 h3 text-left">ลูกค้าของเรา</h2>
            <!--First row-->
            <div class="row features-small mt-1 wow fadeIn text-center">
                @foreach ($customers as $customer)
                <!--Grid column-->
                <div class="col-xl-2 col-lg-6 my-3">
                    <a href="{{ $customer->link }}" target="_blank">
                        <img class="img-fluid" src="{{Voyager::image($customer->thumbnail('small','logo'))}}" alt="">
                        {{-- <p class="grey-text mt-2">{{ $customer->details }}
                        </p> --}}
                    </a>
                </div>
                <!--/Grid column-->
                @endforeach
            </div>
            @endif
        </section>
        <!--Section: More-->
    </div>
</main>
@stop
@section('footer')
@include('frontend.layouts.footer')
@stop
