<meta name="description" content="{{ $seo->meta_description ??  setting('web-seo.description') }}" />
<meta name="keywords" content="{{ $seo->meta_keywords ?? setting('web-seo.keyword') }}" />
<meta name="author" content="Sansu Sa Ma Air" />
<!-- Open Graph data -->
<meta property="og:url" content="{{ url()->full() }}" />
<meta property="og:type" content="webiste" />
<meta property="og:title" content="{{ $seo->seo_title ?? setting('web-seo.title') }}" />
<meta property="og:description" content="{{ $seo->meta_description ?? setting('web-seo.description') }}" />
<meta property="og:image"
    content="{{ (isset($seo->image) && Voyager::image($seo->image)) ? Voyager::image($seo->thumbnail('medium')) : Voyager::image(setting('web-seo.image')) }}" />
<meta property="og:site_name" content="{{ $seo->title ?? setting('web-seo.title') }}" />
<!-- Twitter Card data -->
<meta name="twitter:card" content="{{ $seo->seo_title ??  setting('web-seo.title') }}">
<meta name="twitter:site" content="{{ url()->full() }}">
<meta name="twitter:title" content="{{ $seo->seo_title ?? setting('site.title') }}">
<meta name="twitter:description" content="{{ $seo->meta_description ?? setting('web-seo.description') }}">
<meta name="twitter:creator" content="">
<!-- Twitter Summary card images must be at least 120x120 -->
<meta name="twitter:image"
    content="{{ (isset($seo->image) && Voyager::image($seo->image)) ? Voyager::image($seo->thumbnail('medium')) : Voyager::image(setting('web-seo.image')) }}">
