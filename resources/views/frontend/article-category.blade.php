@extends('frontend.main')
@section('title', setting('site.title'))
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
@isset($banners)
{{-- @include('frontend.slide.banner-video')  --}}
{{-- @include('frontend.slide.banner-image') --}}
@endisset
@stop
@section('content')
<main class="mt-0">
    <div class="site-main-container">
        <section class="top-post-area pt-10">
            <div class="container no-padding">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hero-nav-area">
                            <h1 class="text-white">{{ $category }}</h1>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="news-tracker-wrap">
                            <h6><span><a href="{{ url('/') }}"> ข่าวหน้าแรก </a></span> <span
                                    class="lnr lnr-arrow-right"></span>
                                {{ $category }}
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="latest-post-area pb-120">
            <div class="container no-padding">
                <div class="row">
                    <div class="col-lg-8 post-list">
                        <!-- Start latest-post Area -->
                        <div class="latest-post-wrap">
                            <h4 class="cat-title">ข่าวล่าสุด</h4>
                            @if(sizeof($article) > 0)
                            @foreach ($article as $news)
                            <div class="single-latest-post row align-items-center">
                                <div class="col-lg-5 post-left">
                                    <div class="feature-img relative">
                                        <div class="overlay overlay-bg"></div>
                                        <img class="img-fluid" src="{{ Voyager::image($news->image) }}" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-7 post-right">
                                    <a href="{{ route('news.show',$news->slug) }}">
                                        <h4>{{ $news->title }}</h4>
                                    </a>
                                    <ul class="meta">
                                        <li><a href="#"><span
                                                    class="lnr lnr-calendar-full"></span>{{ thai_date_fullmonth(strtotime($news->public_at)) }}</a>
                                        </li>
                                    </ul>
                                    <p class="excert">
                                        {{ $news->excerpt }}
                                    </p>
                                </div>
                            </div>
                            @endforeach
                            @endif
                            <div class="load-more">
                                {!! $article->links() !!}
                            </div>
                        </div>
                        <!-- End latest-post Area -->
                    </div>
                    <div class="col-lg-4">
                        @if($e_newsall)
                        <div class="sidebars-area">
                            <div class="single-sidebar-widget editors-pick-widget">
                                <h6 class="title">หนังสือพิมพ์ไทยแหลมทอง</h6>
                                <div class="editors-pick-post">
                                    @foreach ($e_newsall as $key => $e_news)
                                    @if($key == 0)
                                    <div class="feature-img-wrap relative">
                                        <div class="feature-img relative">
                                            <div class="overlay"></div>
                                            <a href="{{ route('e-book.show',$e_news->slug) }}">
                                                <img class="img-fluid" src="{{ Voyager::image($e_news->cover_page) }}"
                                                    alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="details">
                                        <a href="{{ route('e-book.show',$e_news->slug) }}">
                                            <h4 class="mt-20">{{ $e_news->title }}</h4>
                                        </a>
                                        <ul class="meta">
                                            <li><a href="#"><span
                                                        class="lnr lnr-calendar-full"></span>{{ thai_date_fullmonth(strtotime($e_news->public_at)) }}</a>
                                            </li>
                                        </ul>
                                    </div>
                                    @endif
                                    @endforeach
                                    <div class="post-lists">
                                        @foreach ($e_newsall as $key => $e_news)
                                        @if($key > 0)
                                        <div class="single-post d-flex flex-row">
                                            <div class="detail">
                                                <a href="{{route('e-book.show',$e_news->slug) }}">
                                                    <h6>{{$e_news->title}}</h6>
                                                </a>
                                                <ul class="meta">
                                                    <li><a href="{{route('e-book.show',$e_news->slug) }}"><span
                                                                class="lnr lnr-calendar-full"></span>{{ thai_date_fullmonth(strtotime($e_news->public_at)) }}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="single-sidebar-widget ads-widget">
                                <img class="img-fluid" src="img/sidebar-ads.jpg" alt="">
                            </div>
                            <div class="single-sidebar-widget newsletter-widget">
                                <h6 class="title">ค้นหา</h6>
                                <div class="form-group d-flex flex-row">
                                    <div class="col-autos">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="ค้นหาข่าว" type="text">
                                        </div>
                                    </div>
                                    <a href="#" class="bbtns">ตกลง</a>
                                </div>
                            </div>
                            <div class="single-sidebar-widget most-popular-widget">
                                <h6 class="title">หมวดหมู่ข่าว</h6>
                                @foreach ($categories as $category)
                                <div class="single-list flex-row d-flex">
                                    <div class="details">
                                        <a href="{{ route('news.category',$category->slug) }}">
                                            <h6>{{$category->name}}</h6>
                                        </a>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="single-sidebar-widget social-network-widget">
                                <img class="img-fluid" src="img/sidebar-ads.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@stop
@section('footer')
@include('frontend.layouts.footer')
@stop
