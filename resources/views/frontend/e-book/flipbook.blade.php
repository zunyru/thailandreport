<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('plugin/flipbook/css/flipbook.style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugin/flipbook/css/font-awesome.css')}}">
    <script src="{{ asset('plugin/flipbook/js/flipbook.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
        $("#container").flipBook({
            pages:[
                {src:"{{ asset('plugin/flipbook/images/book1/1.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/1.jpg') }}", title:"Cover"},
                {src:"{{ asset('plugin/flipbook/images/book1/2.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/2.jpg') }}", title:"Page two"},
                {src:"{{ asset('plugin/flipbook/images/book1/3.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/3.jpg') }}", title:"Page three"},
                {src:"{{ asset('plugin/flipbook/images/book1/4.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/4.jpg') }}", title:""},
                {src:"{{ asset('plugin/flipbook/images/book1/5.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/5.jpg') }}", title:"Page five"},
                {src:"{{ asset('plugin/flipbook/images/book1/6.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/6.jpg') }}", title:"Page six"},
                {src:"{{ asset('plugin/flipbook/images/book1/7.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/7.jpg') }}", title:"Page seven"},
                {src:"{{ asset('plugin/flipbook/images/book1/8.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/8.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/9.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/9.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/10.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/10.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/11.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/11.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/12.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/12.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/13.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/13.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/14.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/14.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/15.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/15.jpg') }}'", title:"Last"},
                {src:"{{ asset('plugin/flipbook/images/book1/16.jpg') }}",thumb:"{{ asset('plugin/flipbook/images/book1/16.jpg') }}'", title:"Last"},
            ],
            backgroundColor:"#ccc",
            sideMenuOverBook:true,
            responsiveViewTreshold:480
        });
    })
    </script>
</head>

<body>
    <div id="container" />
</body>

</html>
