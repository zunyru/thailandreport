@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('content')
<div style="height: 600px">
    <div id="container" style="position:relative;width:100%;height:100%" />
</div>
@stop
@push('custom-scripts')
<script type="text/javascript">
    @php
        $ebooks = json_decode($e_news->ebook);
        @endphp
        $(document).ready(function () {
        $("#container").flipBook({
            pages:[
                @foreach ($ebooks as $key => $image)
                {
                    src:"{{ Voyager::image( $image ) }}",
                    thumb:"{{ Voyager::image( $image ) }}",
                    title:"หน้า {{ $key+1 }}"

                },
                @endforeach
            ],
            backgroundColor:"#ccc",
            sideMenuOverBook:true,
            responsiveView:true,
            wheelDisabledNotFullscreen:true,
            btnSound:{
                enabled: true,
                title: "Volume",
                icon: "fa-volume-up",
                iconAlt: "fa-volume-off",
                icon2: "volume_up",
                iconAlt2: "volume_mute",
                hideOnMobile:false
            },
            // layout:3,
            // btnSound:{vAlign:"top", hAlign:"left"},
            // btnAutoplay:{vAlign:"top", hAlign:"left"},
            // currentPage:{vAlign:"bottom", hAlign:"left"}

        });
    })
</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@stop
