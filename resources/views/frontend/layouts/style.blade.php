<!--CSS ============================================= -->
<link rel="stylesheet" href="{{ asset('css/linearicons.css') }}">
<link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/nice-select.css') }}">
<link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('css/main.css') }}">
{{-- E-book --}}
<link rel="stylesheet" type="text/css" href="{{ asset('plugin/flipbook/css/flipbook.style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugin/flipbook/css/font-awesome.css')}}">
{{-- end E-book--}}
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
@if(setting('site.google-font'))
<link href="https://fonts.googleapis.com/css?family={{ setting('site.google-font') }}:400,500,700&display=swap"
    rel="stylesheet">
<style type="text/css">
    html,
    body,
    header,
    .view,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: "{{ setting('site.google-font') }}";
    }
</style>
@endif
<style type="text/css">
{{-- Custon css --}}
@if(setting('css.custom_css'))
{!! setting('css.custom_css') !!}
@endif
/* Necessary for full page carousel*/
html,
body,
header,
.view {
height: auto;
}
body {
color: @php echo setting('site.text_body_color')? setting('site.text_body_color'): '#212529'@endphp;
background: @php echo setting('site.body_background_color')? setting('site.body_background_color'): ''@endphp;
}
a {
color: @php echo setting('site.text_link_color')? setting('site.text_link_color'): '#007bff'@endphp;
}
.grey-text {
color: @php echo setting('site.text_sub_color')? setting('site.text_sub_color').'!important': '#9e9e9e
!important;'@endphp;
}
.show>.btn-primary.dropdown-toggle,
.btn-primary {
background-color: @php echo setting('site.button_color')? setting('site.button_color').'!important': '#4285f4
!important;'@endphp;
color: @php echo setting('site.button_text_color')? setting('site.button_text_color'): '#fff'@endphp;
;
}
/* Carousel*/
.carousel,
.carousel-item,
.carousel-item.active {
height: 100%;
}
.carousel-inner {
height: 100%;
}
@php $color = setting('site.menu_bar_background_color') ? setting('site.menu_bar_background_color') : '#1C2331'; @endphp
.form-control:focus {
border-color: {{ setting('site.menu_bar_background_color') ? setting('site.menu_bar_background_color') : '#1C2331' }};
outline: 0;
box-shadow: 0 0 0 0.2rem {{ hex2rgba($color,0.25) }};
}
@media (min-width: 800px) and (max-width: 850px) {
.navbar:not(.top-nav-collapse) {
background: #1C2331 !important;
}
}
@stack('css')
</style>
