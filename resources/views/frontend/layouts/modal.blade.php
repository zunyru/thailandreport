{{-- modal --}}
@if(setting('site.img-event'))
<div id="myModal" aria-hidden="true" aria-labelledby="myModalLabel" class="modal fade" id="modalIMG" role="dialog"
    tabindex="-1">
    <div class="modal-dialog " role="document">
        {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button> --}}
        <div class="modal-content">
            <div class="modal-body mb-0 p-0">
                <img class="img-fluid" src="{{Voyager::image(setting('site.img-event'))}}" />
            </div>
            <div class="modal-footer">
                <div class="checkbox pull-right">
                    <label>
                        <input class='modal-check' name='modal-check' type="checkbox">
                        ไม่ต้องแสดงอีก
                    </label>
                    <button class="btn btn-outline-danger btn-rounded btn-md ml-4 text-center" data-dismiss="modal"
                        type="button">ปิด</button>
                </div>
            </div>
        </div>
    </div>
</div>
@push('custom-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script>
    $(document).ready(function(){
    //Referances
    //jQuery Cookie : https://github.com/carhartl/jquery-cookie
    //Modal : http://getbootstrap.com/javascript/#modals
    var my_cookie = $.cookie($('.modal-check').attr('name'));
    if (my_cookie && my_cookie == "true") {
        $(this).prop('checked', my_cookie);
        console.log('checked checkbox');
    }
    else{
        $('#myModal').modal('show');
        console.log('uncheck checkbox');
    }

    $(".modal-check").change(function() {
        $.cookie($(this).attr("name"), $(this).prop('checked'), {
            //path: '/',
            expires: 1
        });
    });
});
</script>
@endpush
@endif
