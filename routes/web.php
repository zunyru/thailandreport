<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\MainController@index')->name('home');

Route::get('/page/{slug}', 'Frontend\MainController@show')->name('page');

Route::get('/services', 'Frontend\ServiceController@index')->name('services.index');

Route::get('/services/{slug}', 'Frontend\ServiceController@show')->name('services.show');

Route::get('/news', 'Frontend\ArticleController@index')->name('news.index');

Route::get('/news/{slug}', 'Frontend\ArticleController@show')->name('news.show');

Route::get('/news/tag/{slug}', 'Frontend\ArticleController@showTag')->name('news.tag');

Route::get('/news/category/{category}', 'Frontend\ArticleController@category')->name('news.category');

Route::get('/about-us', 'Frontend\AboutusController@index')->name('about-us.index');

Route::get('/contact-us', 'Frontend\ContactController@index')->name('contact-us.index');

Route::post('/contact-us', 'Frontend\ContactController@store')->name('contact-us.store');

// Route::get('/e-book', function () {
//     return view('frontend.e-book.flipbook');
// })->name('e-book.index');

Route::get('/e-book/{slug}', 'Frontend\MainController@show')->name('e-book.show');

Route::get('/e-book', function () {
    $e_news =  App\Enews::orderBy('updated_at', 'desc')->first();
    return view('frontend.e-book.e-news', compact('e_news'));
})->name('e-book.index');


Route::get('/search', 'Frontend\MainController@search')->name('search');
