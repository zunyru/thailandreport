<!DOCTYPE html>
<html lang="th">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <?php $admin_favicon = Voyager::setting('site.favicon', ''); ?>
    @if($admin_favicon == '')
    <link rel="shortcut icon" href="{{ voyager_asset('images/logo-icon.png') }}" type="image/png">
    @else
    <link rel="shortcut icon" href="{{ Voyager::image($admin_favicon) }}" type="image/png">
    @endif
    <title>@yield('title')</title>
    @yield('seo')
    @include('frontend.layouts.style')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-159576532-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-159576532-2');
    </script>
    <script data-ad-client="ca-pub-2515169839077156" async
        src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body>
    @section('navbar')
    @show
    @section('slides')
    @show
    <!-- data-ride="carousel" -->
    <!--Main layout-->
    @yield('content')
    <!--Main layout-->
    @section('footer')
    @show
    <!-- SCRIPTS -->
    @include('frontend.layouts.scripts')
    @stack('custom-scripts')
</body>

</html>
