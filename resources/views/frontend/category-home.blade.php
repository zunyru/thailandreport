<div class="single-sidebar-widget most-popular-widget">
    <h6 class="title">หมวดหมู่ข่าว</h6>
    <div class="single-list flex-row d-flex">
        <div class="details">
            <a href="{{ url('/') }}">
                <h6>หน้าแรก</h6>
            </a>
        </div>
    </div>
    <div class="single-list flex-row d-flex">
        <div class="details">
            <a href="{{ route('e-book.index') }}">
                <h6>หนังสือพิมพ์</h6>
            </a>
        </div>
    </div>
    <div class="single-list flex-row d-flex">
        <div class="details">
            <a href="{{ route('news.category','ทั่วไป') }}">
                <h6>ทั่วไป</h6>
            </a>
        </div>
    </div>
    <div class="single-list flex-row d-flex">
        <div class="details">
            <a href="{{ route('news.category','การเมือง') }}">
                <h6>การเมือง</h6>
            </a>
        </div>
    </div>
    <div class="single-list flex-row d-flex">
        <div class="details">
            <a href="{{ route('news.category','เศรษฐกิจ') }}">
                <h6>เศรษฐกิจ</h6>
            </a>
        </div>
    </div>
    <div class="single-list flex-row d-flex">
        <div class="details">
            <a href="{{ route('news.category','ไลฟ์สไตล์-บันเทิง') }}">
                <h6>ไลฟ์สไตล์-บันเทิง</h6>
            </a>
        </div>
    </div>
    <div class="single-list flex-row d-flex">
        <div class="details">
            <a href="{{ url('/') }}">
                <h6>ภาคใต้</h6>
            </a>
        </div>
    </div>
    <div class="single-list flex-row d-flex">
        <div class="details">
            <a href="{{ url('/') }}">
                <h6>ต่างประเทศ</h6>
            </a>
        </div>
    </div>
    <div class="single-list flex-row d-flex">
        <div class="details">
            <a href="{{ route('news.category','ภาพข่าวสังคม')}}">
                <h6>ภาพข่าวสังคม</h6>
            </a>
        </div>
    </div>
</div>
