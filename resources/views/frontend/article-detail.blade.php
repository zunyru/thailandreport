@extends('frontend.main')
@section('title', $article->title ?? setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@if(setting('web-seo.sharethis'))
{!! setting('web-seo.sharethis') !!}
@endif
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@endisset
@stop
@section('content')
<main class="mt-0">
    <div class="container detail pt-3">
        <section class="content">
            <h1 class="h3 text-left mb-3">{{ $article->title }}</h1>
            <p class="category">หมวดหมู่ :
                @foreach ($article->categories as $categories)
                {{ $categories->name }},
                @endforeach
            </p>
            <small class="post_date">โฟสเมื่อ : {{ DateThai($article->created_at)}}</small>
            <small class="read">อ่าน : {{ number_format($article->viewer + 1 ,0)}}</small>
            {{-- tag --}}
            <section class="mt-3">
                @foreach ($article->tagArray as $tag)
                <a href="{{ route('news.tag',str_replace(' ','-',$tag))  }}" class="tag">{{ $tag }}</a>
                @endforeach
            </section>
            <div class="row">
                <div class="col-md-8 mb-5 mt-3">
                    <img src="{{ Voyager::image( $article->image) }}" class="img-fluid rounded-lg"
                        alt="{{ $article->title }}">
                    {{-- Share this --}}
                    @if(setting('web-seo.sharethis'))
                    <div class="my-4">
                        <div class="sharethis-inline-share-buttons"></div>
                    </div>
                    @endif
                    <div class="mt-2">
                        {!! $article->body !!}
                    </div>
                </div>
                <div class="col-md-4 mt-3">
                    @if(setting('contact.facebook_page'))
                    {!! setting('contact.facebook_page') !!}
                    @endif
                    <div class="row">
                        <div class="ads col ml-3 mt-3">
                            {{-- ads rigth --}}
                            <script id="11376026259821">
                                (function(e){var js=document.getElementById("11376026259821"); var block=document.createElement("div"); block.id=parseInt(Math.random()*1e9).toString(16)+e; js.parentNode.insertBefore(block,js); if("undefined"===typeof window.loaded_blocks_yengo){window.loaded_blocks_yengo=[]; function n(){var e=window.loaded_blocks_yengo.shift(); var t=e.adp_id; var r=e.div; var i=document.createElement("script"); i.async=true; i.charset="utf-8"; var as=(typeof __da_already_shown!="undefined")?"&as="+__da_already_shown.slice(-20).join(":"):""; i.src="https://code.yengo.com/data/"+t+".js?async=1&div="+r+"&t="+Math.random()+as; var s=document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]; var o; s.appendChild(i); i.onload=function(){o=setInterval(function(){if(document.getElementById(r).innerHTML && window.loaded_blocks_yengo.length){n(); clearInterval(o)}},50)}; i.onerror=function(){o=setInterval(function(){if(window.loaded_blocks_yengo.length){n(); clearInterval(o)}},50)}; } setTimeout(n)}window.loaded_blocks_yengo.push({adp_id: e,div: block.id})})(259821)
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@stop
@push('custom-scripts')
<script>
    $(".fancybox").fancybox();
</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@stop
