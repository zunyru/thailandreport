@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('content')
<div class="site-main-container">
    <!-- Start top-post Area -->
    <section class="top-post-area pt-10">
        <div class="container no-padding">
            <div class="row small-gutters">
                @foreach ($hotnews as $key => $news)
                @if($key == 0)
                <div class="col-lg-8 top-post-left">
                    <a href="{{ route('news.show',$news->slug) }}">
                        <div class="feature-image-thumb relative">
                            <div class="overlay overlay-bg"></div>
                            <img class="img-fluid" src="{{ Voyager::image($news->image) }}" alt="">
                        </div>
                        <div class="top-post-details">
                            <h3>{{ $news->title }}</h3>
                            <ul class="meta">
                                <li>
                                    <span
                                        class="lnr lnr-calendar-full"></span>{{ thai_date_fullmonth(strtotime($news->public_at)) }}
                                </li>
                            </ul>
                        </div>
                    </a>
                </div>
                @endif
                @endforeach
                <div class="col-lg-4 top-post-right">
                    @foreach ($hotnews as $key => $news)
                    @if($key > 0)
                    <div class="single-top-post">
                        <a href="{{ route('news.show',$news->slug) }}">
                            <div class="feature-image-thumb relative">
                                <div class="overlay overlay-bg"></div>
                                <img class="img-fluid" src="{{ Voyager::image($news->thumbnail('medium')) }}" alt="">
                            </div>
                            <div class="top-post-details">
                                <h4>{{ $news->title }}</h4>
                                <ul class="meta">
                                    <li>
                                        <span
                                            class="lnr lnr-calendar-full"></span>{{ thai_date_fullmonth(strtotime($news->public_at)) }}
                                    </li>
                                </ul>
                            </div>
                        </a>
                    </div>
                    @endif
                    @endforeach
                </div>
                <div class="col-lg-12">
                    <div class="news-tracker-wrap">
                        <h6><span>ไทยแหลมทอง:</span> <a href="#">อ่าวข่าวสารน่าสนใจได้แล้วที่นี่</a></h6>
                    </div>
                </div>
                {{-- <div class="col-lg-12">
                    <div class="news-tracker-wrap">
                        <img class="img-fluid" src="{{ Storage::url('ads/phuketguide.jpg')}}"" alt="">
                    </div>
                </div> --}}
            </div>
        </div>
    </section>
    <!-- End top-post Area -->
    <!-- Start latest-post Area -->
    <section class=" latest-post-area pb-120">
                        <div class="container no-padding">
                            <div class="row">
                                <div class="col-lg-8 post-list">
                                    <!-- Start latest-post Area -->
                                    <div class="latest-post-wrap">
                                        <h4 class="cat-title">ข่าวล่าสุด</h4>
                                        @foreach ($lastnews as $key => $news)
                                        <a href="{{ route('news.show',$news->slug) }}">
                                            <div class="single-latest-post row align-items-center">
                                                <div class="col-lg-5 post-left">
                                                    <div class="feature-img relative">
                                                        <div class="overlay overlay-bg"></div>
                                                        <img class="img-fluid"
                                                            src="{{ Voyager::image($news->thumbnail('medium')) }}"" alt="">
                                                        </div>

                                                    </div>
                                                <div class=" col-lg-7 post-right">
                                                        <h4>{{ $news->title }}</h4>
                                                        <ul class="meta">
                                                            <li>
                                                                <span
                                                                    class="lnr lnr-calendar-full"></span>{{ thai_date_fullmonth(strtotime($news->public_at)) }}
                                                            </li>
                                                        </ul>
                                                        <p class="excert">
                                                            {{ $news->excerpt }}
                                                        </p>
                                                    </div>
                                                </div>
                                        </a>
                                        @endforeach
                                    </div>
                                    <!-- End latest-post Area -->
                                    <!-- Start banner-ads Area -->
                                    <div class="col-lg-12 ad-widget-wrap mt-30 mb-30">
                                        <script id="3922157915259691">
                                            (function(e){var js=document.getElementById("3922157915259691"); var block=document.createElement("div"); block.id=parseInt(Math.random()*1e9).toString(16)+e; js.parentNode.insertBefore(block,js); if("undefined"===typeof window.loaded_blocks_yengo){window.loaded_blocks_yengo=[]; function n(){var e=window.loaded_blocks_yengo.shift(); var t=e.adp_id; var r=e.div; var i=document.createElement("script"); i.async=true; i.charset="utf-8"; var as=(typeof __da_already_shown!="undefined")?"&as="+__da_already_shown.slice(-20).join(":"):""; i.src="https://code.yengo.com/data/"+t+".js?async=1&div="+r+"&t="+Math.random()+as; var s=document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]; var o; s.appendChild(i); i.onload=function(){o=setInterval(function(){if(document.getElementById(r).innerHTML && window.loaded_blocks_yengo.length){n(); clearInterval(o)}},50)}; i.onerror=function(){o=setInterval(function(){if(window.loaded_blocks_yengo.length){n(); clearInterval(o)}},50)}; } setTimeout(n)}window.loaded_blocks_yengo.push({adp_id: e,div: block.id})})(259691)
                                        </script>
                                    </div>
                                    <!-- End banner-ads Area -->
                                    <!-- Start popular-post Area -->
                                    <div class="popular-post-wrap">
                                        <h4 class="title">ข่าวฮิต</h4>
                                        @foreach ($popularnews as $key => $news)
                                        @if($key == 0)
                                        <div class="feature-post relative">
                                            <a href="{{ route('news.show',$news->slug) }}">
                                                <div class="feature-img relative">
                                                    <div class="overlay overlay-bg"></div>
                                                    <img class="img-fluid" src="{{ Voyager::image($news->image) }}"
                                                        alt="">
                                                </div>
                                                <div class="details">
                                                    <h3>{{ $news->title }}</h3>
                                                    <li>
                                                        <ul class="meta">
                                                            <li><a href="{{ route('news.show',$news->slug) }}"><span
                                                                        class="lnr lnr-calendar-full"></span>{{ thai_date_fullmonth(strtotime($news->public_at)) }}</a>
                                                            </li>
                                                        </ul>
                                                </div>
                                            </a>
                                        </div>
                                        @endif
                                        @endforeach
                                        <div class="row mt-20 medium-gutters">
                                            @foreach ($popularnews as $key => $news)
                                            @if($key > 0)
                                            <div class="col-lg-6 single-popular-post">
                                                <a href="{{ route('news.show',$news->slug) }}">
                                                    <div class="feature-img-wrap relative">
                                                        <div class="feature-img relative">
                                                            <div class="overlay overlay-bg"></div>
                                                            <img class="img-fluid"
                                                                src="{{ Voyager::image($news->thumbnail('medium')) }}"
                                                                alt="">
                                                        </div>
                                                    </div>
                                                    <div class="details">
                                                        <h4>{{ $news->title }}</h4>
                                                        <ul class="meta">
                                                            <li><a href="#"><span
                                                                        class="lnr lnr-calendar-full"></span>{{ thai_date_fullmonth(strtotime($news->public_at)) }}</a>
                                                            </li>
                                                        </ul>
                                                        <p class="excert">
                                                            {{ $news->excerpt }}
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>
                                            @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- End popular-post Area -->
                                    <a target="_bank" href="https://www.facebook.com/sukaifashion">
                                    <img class="img-fluid" src="{{Storage::url('ads/sa.jpg')}}" alt="">
                                    </a>
                                    <!-- Start relavent-story-post Area -->
                                    <div class="relavent-story-post-wrap mt-30">
                                        <h4 class="title">ข่าวทั่วไป</h4>
                                        <div class="relavent-story-list-wrap">
                                            @foreach ($allnews as $news)
                                            <div class="single-relavent-post row align-items-center">
                                                <div class="col-lg-5 post-left">
                                                    <div class="feature-img relative">
                                                        <div class="overlay overlay-bg"></div>
                                                        <a href="{{ route('news.show',$news->slug) }}">
                                                            <img class="img-fluid"
                                                                src="{{ Voyager::image($news->thumbnail('medium')) }}"
                                                                alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-7 post-right">
                                                    <a href="{{ route('news.show',$news->slug) }}">
                                                        <h4>{{ $news->title }}</h4>
                                                        <ul class="meta">
                                                            <li><span
                                                                    class="lnr lnr-calendar-full"></span>{{ thai_date_fullmonth(strtotime($news->public_at)) }}
                                                            </li>
                                                        </ul>
                                                        <p class="excert">
                                                            {{ $news->excerpt }}
                                                        </p>
                                                    </a>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- End relavent-story-post Area -->
                                </div>
                                <div class="col-lg-4">
                                    @if($e_newsall)
                                    <div class="sidebars-area">
                                        <div class="single-sidebar-widget editors-pick-widget">
                                            <h6 class="title">หนังสือพิมพ์ไทยแหลมทอง</h6>
                                            <div class="editors-pick-post">
                                                @foreach ($e_newsall as $key => $e_news)
                                                @if($key == 0)
                                                <a href="{{ route('e-book.show',$e_news->slug) }}">
                                                    <div class="feature-img-wrap relative">
                                                        <div class="feature-img relative">
                                                            <div class="overlay"></div>
                                                            <img class="img-fluid"
                                                                src="{{ Voyager::image($e_news->cover_page) }}" alt="">
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="details">
                                                    <a href="{{ route('e-book.show',$e_news->slug) }}">
                                                        <h4 class="mt-20">{{ $e_news->title }}</h4>
                                                    </a>
                                                    <ul class="meta">
                                                        <li><a href="#"><span
                                                                    class="lnr lnr-calendar-full"></span>{{ thai_date_fullmonth(strtotime($e_news->public_at)) }}</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                @endif
                                                @endforeach
                                                <div class="post-lists">
                                                    @foreach ($e_newsall as $key => $e_news)
                                                    @if($key > 0)
                                                    <div class="single-post d-flex flex-row">
                                                        <div class="detail">
                                                            <a href="{{route('e-book.show',$e_news->slug) }}">
                                                                <h6>{{$e_news->title}}</h6>
                                                            </a>
                                                            <ul class="meta">
                                                                <li><a href="{{route('e-book.show',$e_news->slug) }}"><span
                                                                            class="lnr lnr-calendar-full"></span>{{ thai_date_fullmonth(strtotime($e_news->public_at)) }}</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="single-sidebar-widget ads-widget">
                                            <img class="img-fluid" src="{{ Storage::url('ads/glod.jpg') }}" alt="">
                                        </div>
                                        <div class="single-sidebar-widget newsletter-widget">
                                            <h6 class="title">ค้นหา</h6>
                                            <p>
                                            </p>
                                            <div class="form-group d-flex flex-row">
                                                <form action="{{ route('search') }}" method="GET">
                                                    {{ csrf_field() }}
                                                    <div class="col-autos" style="display: inline-flex;">
                                                        <div class="input-group">
                                                            <input class="form-control" placeholder="ค้นหาข่าว"
                                                                type="text" name="search">
                                                        </div>
                                                        <button type="" class="btn bbtns">ตกลง</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        @include('frontend.category-home')
                                        <div class="single-sidebar-widget social-network-widget">
                                            <img class="img-fluid" src="{{Storage::url('ads/coca.jpg')}}" alt="">
                                        </div>
                                        <div class="single-sidebar-widget social-petroleum-widget text-center">
                                            <div class="mt-5">
                                                <h6 class="title">ราคาน้ำมันวันนี้</h6>
                                                <IFRAME height="410" width="300" frameborder="0"
                                                    SRC="https://www.bangchak.co.th/th/OilPriceWidget"></IFRAME>
                                            </div>
                                            <div class="mt-5">
                                                <h6 class="title">ราคาทองคำวันนี้</h6>
                                                <iframe src="https://namchiang.com/ncgp2-1.swf" width="172" height="400"
                                                    frameborder="0" marginheight=0 marginwidth=0
                                                    scrolling="no"></iframe>
                                            </div>
                                            <div class="mt-3">
                                                <h6 class="title">ผลรางวัลสลากกินแบ่งรัฐบาล</h6>
                                                <iframe src="https://www.lottery.co.th/small" width="210" height="290"
                                                    frameborder="0"></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
    </section>
    <!-- End latest-post Area -->
    @include('frontend.layouts.modal')
</div>
@stop
@section('footer')
@include('frontend.layouts.footer')
@stop
