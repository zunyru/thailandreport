@push('css')
.navbar{
background-color:{{ setting('site.menu_bar_background_color')? setting('site.menu_bar_background_color') : '#1C2331' }};
}
.navbar.navbar-dark .breadcrumb .nav-item.active>.nav-link:hover, .navbar.navbar-dark .navbar-nav
.nav-item.active>.nav-link:hover, .navbar.navbar-dark .navbar-toggler,
.navbar.navbar-dark .breadcrumb .nav-item .nav-link,
.navbar.navbar-dark .navbar-nav .nav-item .nav-link {
color: {{ setting('site.menu_bar_text_color')? setting('site.menu_bar_text_color'): '#fff' }};
border-radius: 4px;
}
span.hamberger{
background-color:{{ setting('site.menu_bar_text_color')? setting('site.menu_bar_text_color') : '#fff' }};
}
@php $color_text = setting('site.menu_bar_text_color') ? setting('site.menu_bar_text_color') : '#fff'; @endphp
.navbar.navbar-dark .breadcrumb .nav-item .nav-link:hover, .navbar.navbar-dark .navbar-nav .nav-item .nav-link:hover {
color: {{ hex2rgba($color_text,0.75) }};
}
@endpush
<!-- Navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
    <div class="container">
        <!-- Brand -->
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ setting('site.logo') ? Voyager::image(setting('site.logo')) : asset('img/logo.png') }}">
        </a>
        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            {{-- <span class="navbar-toggler-icon"></span> --}}
            <span class="navbar-toggler-icon hamberger"></span>
            <span class="navbar-toggler-icon hamberger"></span>
            <span class="navbar-toggler-icon hamberger"></span>
        </button>
        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left -->
            {{ menu('web','menu.my_menu') }}
            <!-- Right -->
        </div>
    </div>
</nav>
<!-- Navbar -->
