<!-- JQuery -->
<script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{ asset('js/mdb.min.js') }}"></script>
{{-- fancybox --}}
<script type="text/javascript" src="{{ asset('plugin/fancybox/source/jquery.fancybox.pack.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugin/fancybox/source/helpers/jquery.fancybox-buttons.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugin/fancybox/source/helpers/jquery.fancybox-media.js') }}"></script>
<!-- Initializations -->
<script type="text/javascript">
    // Animations initialization
    new WOW().init();
</script>
<script src="//cdn.rawgit.com/hilios/jQuery.countdown/2.2.0/dist/jquery.countdown.min.js"></script>
<script>
    var datetime = $('#clock').attr('data-datetime');
    $('#clock').countdown(datetime)
        .on('update.countdown', function(event) {
        var format = '%H:%M:%S';
        if(event.offset.totalDays > 0) {
            format = '%-d day%!d ' + format;
        }
        if(event.offset.weeks > 0) {
            format = '%-w week%!w ' + format;
        }
        $(this).html(event.strftime(format));
        })
        .on('finish.countdown', function(event) {
        $(this).html('This offer has expired!')
            .parent().addClass('disabled');
        });
</script>
{{-- Custon js --}}
@if(setting('js.custom_js'))
$( document ).ready(function() {
console.log( "custom_js ready!" );
{!! setting('js.custom_js') !!}
});
@endif
