<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class Enews extends Model
{
    const PUBLISHED = 'PUBLISHED';

    public function scopePublished(Builder $query)
    {
        return $query->where('status', '=', static::PUBLISHED);
    }

    public function scopePublicat(Builder $query)
    {
        return $query->where('public_at', '<=', \Carbon\Carbon::now());
    }
}
